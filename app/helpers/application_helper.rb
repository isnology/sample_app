module ApplicationHelper
  
  # return the full title on a per-page basis
  def full_title(page_title = '')
    (page_title.empty? ? '' : page_title + ' | ') + 'Ruby on Rails Tutorial Sample App'
  end
  
end
