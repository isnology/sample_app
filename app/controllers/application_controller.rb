class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def hello
    render html: "Hello world!"
  end
  
  rescue_from ActiveRecord::StaleObjectError do |exception|
    respond_to do |format|
      format.html {
        reload_stale_record_edit_data
        stale_record_recovery_action
      }
      format.xml  { head :conflict }
      format.json { head :conflict }
    end
  end

  protected

    def stale_record_recovery_action
      flash[:danger] = "Another user has made a change to that record "+
          "since you accessed the edit form. You will need to re-enter your changes."
      render :edit, status: :conflict
    end
  
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end
